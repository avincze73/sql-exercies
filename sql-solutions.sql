/*HR DB*/
/*1.
View the data contained in departments table.
*/
SELECT *
FROM   departments;

/*2.
Determine the structure of departments table.
*/
desc departments;

/*3.
Write a query to display the employee id, first name, last name, 
job id, hire date attributes for each employees. Provide 
an alias 'Hire Date' for hire_date column.
*/
select employee_id, last_name, job_id, hire_date as "Hire Date"
from employees;

/*4.
Let us see all unique job ids from employees table!
*/
select distinct job_id from employees;

/*5.
Write a query to display the employee id, first name, 
last name, job id, hire date attributes for each employees. Give 
more descriptive column names!
*/
select employee_id as "Employee Identifier", 
last_name as "Last Name", 
job_id  as JOB_ID, 
hire_date as "Hire Date"
from employees;

/*6.
Give a report of all employees where every row contains the 
last name and the first name of the employee concatenated 
with the job id (with separation character)! 
Name the column Employee And Job.
*/
SELECT last_name || ' ' || first_name || ', ' || job_id "Employee and Job"
FROM   employees;

/*7.
Display the data from employees table. 
Separate each column value by comma. 
The resulting column name is RESULT.
*/
SELECT employee_id || ',' || first_name || ',' || last_name
       || ',' || email || ',' || phone_number || ','|| job_id
|| ',' || manager_id || ',' || hire_date || ','
       || salary || ',' || commission_pct || ',' ||
department_id
       RESULT
FROM   employees;


/*8.
Give a report that contains the last name of the employees earning more than 12000 dollar.
*/
SELECT  last_name, salary
FROM    employees
WHERE   salary > 12000;


/*9.
Present the first name, last name and department identifier for employee with id 101.
*/
SELECT  first_name, last_name, department_id
FROM    employees
WHERE   employee_id = 101;


/*10.
Select employees with salary not between 5000 and 10000 dollar.
*/
SELECT last_name, salary
FROM employees
WHERE salary NOT BETWEEN 5000 AND 10000;

/*11.
Create a report to display the last name, job ID, and hire date for employees 
with the last names of Brown and Martin. Order the query in ascending order by hire date.
*/
SELECT   last_name, job_id, hire_date
FROM     employees
WHERE    last_name IN ('Brown', 'Martin')
ORDER BY hire_date;

/*12.
Select the last name and department id of all employees in 
department 80 or department 90 in ascending alphabetical order by last_name.
*/
SELECT   last_name, department_id
FROM     employees
WHERE    department_id IN (80, 90)
ORDER BY last_name ASC;

/*13.
Display the last name and the salary of employees who earn between 5000 and 12000 dollars
and are in department 80 or department 90. Give meaningful column names! 
*/
SELECT last_name "Employee", 
salary "Monthly Salary" FROM employees
WHERE salary BETWEEN 5000 AND 12000
AND department_id IN (80, 90);


/*14.
Give a report that displays the last name and 
hire date of all employees who were hired in 2018.
*/
SELECT last_name, hire_date
FROM employees
WHERE hire_date >= '18-JAN.-01' AND hire_date <= '18-DEC.-31';

SELECT last_name, hire_date
FROM employees
WHERE hire_date like '18%';

/*15.
Give a report to display the last name and job id of all employees 
who do not have a manager.
*/
SELECT   last_name, job_id
FROM     employees
WHERE    manager_id IS NULL;

/*16.
Create a report to display the last name, salary, and commission for all employees 
who have commissions. Sort the result in descending order of salary and commissions. 
*/
SELECT last_name, salary, commission_pct FROM employees
WHERE commission_pct IS NOT NULL
ORDER BY 2 DESC, 3 DESC;


/*17.
Create a report that displays the last name and salary of employees 
who earn more than an amount that the user specifies after a prompt. 
*/
SELECT  last_name, salary
FROM    employees
WHERE   salary > &sal_amt;

/*18.
Create a query that prompts the user for a manager id, and selects the employee ID, 
last name, salary for that manager�s employees. 
The HR department wants the ability to sort the report on a selected column. 
*/
SELECT employee_id, last_name, salary 
FROM employees
WHERE manager_id = &mgr_num
ORDER BY &order_col;

/*19.
Select the last names of all employees where the fourth letter of the name is 'w'!
*/
SELECT   last_name
FROM     employees
WHERE    last_name LIKE '___w%';


/*20.
Display the last names of all employees who have both an 'a' or an 'e' in their last name.
*/
SELECT   last_name
FROM     employees
WHERE    last_name LIKE '%a%'
OR      last_name LIKE '%e%'
OR      last_name LIKE '%E%'
OR      last_name LIKE '%A%';

/*21.
Display the last name, job, and salary for all employees whose job is that of a CEO or a 
marketing manager, and whose salary is not equal to 2500, 3500, or 7000.
*/
SELECT   last_name, job_id, salary
FROM     employees
WHERE    job_id IN ('CEO', 'MMAN')
AND salary NOT IN (2500, 3500, 7000);

/*22.
Display the last name, salary, and commission for 
all employees whose commission amount is more than 20%. 
*/
SELECT   last_name "Employee", salary "Monthly Salary",
         commission_pct
FROM     employees
WHERE    commission_pct > .20;


/*23.
Display the system date and name the column Date!
*/
SELECT  sysdate "Date"
FROM    dual;

/*24.
Create a report to display the employee number, last name, salary, 
and salary increased by 15.5% 
(expressed as integer) for each employee. Label the column Calculated Salary.
*/
SELECT  employee_id, last_name, salary,
        ROUND(salary * 1.155, 0) "Calculated Salary"
FROM    employees;


/*25.
Create a report to display the employee number, last name, 
salary, and salary increased by 15.5% 
(expressed as integer) and for each employee. Label the 
column Calculated Salary. Add a new column
that substract the old salary from the new one with column label Increase.
*/
SELECT  employee_id, last_name, salary,
        ROUND(salary * 1.155, 0) "New Salary",
        ROUND(salary * 1.155, 0) - salary "Increase"
FROM    employees;




/*26.
Create a report that displays the last name (with the first letter in uppercase and 
all the other letters in lowercase) and the length of the last name for all employees 
whose name starts with the letters 'B' or 'M'. 
Give each column an appropriate label. Sort the results by the employees� last names.
*/
SELECT  INITCAP(last_name) "Name",
        LENGTH(last_name) "Length"
FROM    employees
WHERE   last_name LIKE 'M%'
OR      last_name LIKE 'A%'
ORDER BY last_name;


/*27.
Create a report that displays the last name and calculate the number of months 
between today and the date on which the employee was hired. 
Name the columns as appropriate.
Order the results by the number of months employed.
*/
SELECT last_name, ROUND(MONTHS_BETWEEN(
       SYSDATE, hire_date)) MONTHS_WORKED
FROM   employees
ORDER BY months_worked;


/*28.
Create a query to display the last name and salary for all employees. 
Format the salary to be 10 characters long, left-padded with the dollar symbol.
*/
SELECT last_name,
       LPAD(salary, 10, '$') SALARY
FROM   employees;



/*29.
Create a query that displays employees� last names, and 
indicates the amounts of their salaries with asterisks. 
Each asterisk signifies a thousand dollars. Sort the data in descending order of salary. 
Label the column SALARIES_IN_ASTERISK.
*/
SELECT last_name,
       rpad(' ', (salary/1000)+1, '*')
               SALARIES_IN_ASTERISK
FROM  employees
ORDER BY salary DESC;


/*30.
Create a query to display the last name and the number of weeks 
employed for all employees in department 10. 
Label the number of weeks column as TENURE. Truncate the number of 
weeks value to 0 decimal places. 
Show the records in descending order of the employee�s tenure.
*/
SELECT last_name, trunc((SYSDATE-hire_date)/7) AS TENURE FROM employees
WHERE department_id = 90
ORDER BY TENURE DESC;


/*31.
Display a report that produces the following for each employee:
<employee last name> earns <salary> monthly but wants <3 times salary.>. 
Label the column Dream Salaries.
*/
SELECT  last_name || ' earns '
        || TO_CHAR(salary, 'fm$99,999.00')
        || ' monthly but wants '
        || TO_CHAR(salary * 3, 'fm$99,999.00')
        || '.' "Dream Salaries"
FROM    employees;


/*32.
Display each employee�s last name, hire date, and 'salary review date', 
which is the first Monday after six months of service. 
Label the column REVIEW DATE. 
Format the dates to appear in a format that is similar to 
"Monday, the Thirty-First of July, 2019."
*/
SELECT last_name, hire_date, 
TO_CHAR(NEXT_DAY(ADD_MONTHS(hire_date, 6),'H�tf�'), 
'fmDay, "the" Ddspth "of" Month, YYYY') as "REVIEW DATE"
FROM    employees;

/*33.
Create a query that displays employees� last names and commission amounts. 
If an employee does not earn commission, show �No Commission.� Label the column COMMISSION.
*/
SELECT last_name,
NVL(TO_CHAR(commission_pct), 'No Commission') COMMISSION
FROM   employees;

/*34.
Using the CASE function, write a query that displays the grade of all employees based 
on the value of the JOB_ID column, using the following data:
PRES A, MAN B, PROG C, SREP D, CLERK E, None of the above 0.
*/
SELECT job_id, CASE job_id
               WHEN 'CLERK' THEN 'E'
               WHEN 'SREP'   THEN 'D'
               WHEN 'PROG'  THEN 'C'
               WHEN 'SMAN'   THEN 'B'
               WHEN 'PRES'  THEN 'A'
               ELSE '0'  END  GRADE
FROM employees;


/*35.
with case syntax.
*/
SELECT job_id, CASE
               WHEN job_id = 'CLERK' THEN 'E'
               WHEN job_id = 'SREP'   THEN 'D'
               WHEN job_id = 'PROG'  THEN 'C'
               WHEN job_id = 'SMAN'   THEN 'B'
               WHEN job_id = 'PRES'  THEN 'A'
               ELSE '0'  END  GRADE
FROM employees;


/*36.
with decode syntax.
*/
SELECT job_id, decode (job_id,
'CLERK',  'E',
'SREP',    'D',
'PROG',   'C',
'SMAN',    'B',
'PRES',   'A',
'0') GRADE
FROM employees;

/*Aggregation*/

/*37.
Find the highest, lowest, sum, and average salary of all employees. 
Label the columns Maximum, Minimum, Sum, and Average, respectively. 
Round your results to the nearest whole number.
*/
SELECT ROUND(MAX(salary),0) "Maximum",
       ROUND(MIN(salary),0) "Minimum",
       ROUND(SUM(salary),0) "Sum",
       ROUND(AVG(salary),0) "Average"
FROM   employees;


/*38.
Find the highest, lowest, sum, and average salary for each job type of all employees. 
Label the columns Maximum, Minimum, Sum, and Average, respectively. 
Round your results to the nearest whole number. 
*/
SELECT job_id, ROUND(MAX(salary),0) "Maximum",
               ROUND(MIN(salary),0) "Minimum",
               ROUND(SUM(salary),0) "Sum",
               ROUND(AVG(salary),0) "Average"
FROM   employees
GROUP BY job_id;


/*39.
Write a query to display the number of people with the same job.
*/
SELECT job_id, COUNT(*)
FROM   employees
GROUP BY job_id;

/*40.
Give the number of managers without listing them. 
Label the column Number of Managers.
*/
SELECT COUNT(DISTINCT manager_id) "Number of Managers"
FROM   employees;


/*41.
Find the difference between the 
highest and lowest salaries. Label the column DIFFERENCE.
*/
SELECT   MAX(salary) - MIN(salary) DIFFERENCE
FROM     employees;

/*42.
Display the manager number and the salary of the 
lowest-paid employee for that manager. 
Exclude anyone whose manager is not known. 
Exclude any groups where the minimum salary is 1000 or less. 
Sort the output in descending order of salary.
*/
SELECT manager_id, MIN(salary) FROM employees
WHERE manager_id IS NOT NULL 
GROUP BY manager_id
HAVING   MIN(salary) > 1000
ORDER BY MIN(salary) DESC;


/*43.
Display the total number of employees and, of that total, 
the number of employees hired 
in 2016, 2017, 2018, 2019. Create appropriate column headings.
*/
SELECT COUNT(*) total,
SUM(DECODE(TO_CHAR(hire_date, 'YYYY'),2016,1,0))"2016",
SUM(DECODE(TO_CHAR(hire_date, 'YYYY'),2017,1,0))"2017", 
SUM(DECODE(TO_CHAR(hire_date, 'YYYY'),2018,1,0))"2018", 
SUM(DECODE(TO_CHAR(hire_date, 'YYYY'),2019,1,0))"2019"
FROM    employees;

select TO_CHAR(hire_date, 'YYYY'), count(*)
from employees
/*where TO_CHAR(hire_date, 'YYYY') in ('2016','2017','2018','2019')*/
where TO_CHAR(hire_date, 'YYYY') >= '2016' and   TO_CHAR(hire_date, 'YYYY') <= '2019'
group by TO_CHAR(hire_date, 'YYYY');

/*44.
Create a matrix query to display the job, the salary for that job based on the
department numbers 20, 50, 80, and 90, and the total salary for that job. 
Ensure to give each column an appropriate heading.
*/
SELECT
job_id "Job",
SUM(DECODE(department_id , 20, salary)) "Dept 20", 
SUM(DECODE(department_id , 50, salary)) "Dept 50", 
SUM(DECODE(department_id , 80, salary)) "Dept 80",
SUM(DECODE(department_id , 90, salary)) "Dept 90",
SUM(salary) "Total"
FROM     employees
GROUP BY job_id;

/*Joins*/


/*45.
 Display the addresses of all the departments. 
 Use the LOCATIONS and COUNTRIES tables. 
 Show the location ID, street address, city, state or province, 
 and country in the output. 
 Use a NATURAL JOIN to produce the results
*/
SELECT location_id, street_address, city, state_province, country_name
FROM   locations
NATURAL JOIN  countries;


/*46.
 Report all employees with corresponding departments. 
 Write a query to display the last name, department number, 
 and department name for these employees.
*/
SELECT last_name, department_id, department_name
FROM   employees
JOIN   departments
USING (department_id);


/*47.
 Display the last name, job, department number, and 
 department name for all employees who work in Toronto.
*/
SELECT e.last_name, e.job_id, e.department_id, d.department_name 
FROM employees e 
JOIN departments d
ON (e.department_id = d.department_id)
JOIN locations l
USING  (location_id)
WHERE LOWER(l.city) = 'toronto';

/*48.
Display the employees� last names and employee numbers 
along with their managers� last names and manager numbers.
Name the columns as appropriate.
*/
SELECT w.last_name "Employee", w.employee_id "EMP#", 
m.last_name "Manager", m.employee_id "Mgr#"
FROM   employees w JOIN employees m
ON     (w.manager_id = m.employee_id);


/*49.
Display  employees' last names and employee ids along with 
their managers' last names and employee ids.
*/
SELECT w.last_name "Employee", w.employee_id "EMP#", 
m.last_name "Manager", m.employee_id "Mgr#"
FROM   employees w
LEFT   OUTER JOIN employees m
ON     (w.manager_id = m.employee_id)
ORDER BY 2;

/*50.
Display the employee's last names, department numbers, 
and all employees who work in the same department 
as a given employee. Give each column an appropriate label. 
*/
SELECT e.department_id department, e.last_name employee, c.last_name colleague
FROM employees e JOIN employees c
ON (e.department_id = c.department_id)
WHERE e.employee_id <> c.employee_id
ORDER BY e.department_id, e.last_name, c.last_name;


/*51.
Create a query that displays the name, job, department name, 
salary, and grade for all employees.
*/
SELECT e.last_name, e.job_id, d.department_name,
       e.salary, j.grade_level
FROM   employees e JOIN departments d
ON     (e.department_id = d.department_id)
JOIN   job_grades j
ON    (e.salary BETWEEN j.low_sal AND j.high_sal);


/*52.
The HR department wants to determine the names of all employees 
who were hired after Brown. 
Create a query to display the name and hire date of 
any employee hired after employee Brown.
*/
SELECT e.last_name, e.hire_date
FROM employees e JOIN employees Brown ON (Brown.last_name = 'Brown') 
WHERE Brown.hire_date < e.hire_date;


/*53.
Find the names and hire dates of all employees who were 
hired before their managers, 
along with their managers� names and hire dates. 
*/
SELECT w.last_name, w.hire_date, m.last_name MANAGER, 
m.hire_date "Manager_hire_date"
FROM 
 employees w JOIN employees m
 ON
(w.manager_id = m.employee_id)
 WHERE  w.hire_date <  m.hire_date;
 





